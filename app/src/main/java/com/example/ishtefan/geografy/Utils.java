package com.example.ishtefan.geografy;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ishtefan on 28.10.2016.
 */

public class Utils {

    void removeRepeats(List<Country> countries, String criteria) {

        String prevItem;
        String currItem;

        switch (criteria) {
            case "region": {
                Iterator<Country> iterator = countries.iterator();
                prevItem = iterator.next().region;
                while (iterator.hasNext()) {
                    currItem = iterator.next().region;
                    if (prevItem.equals(currItem)) {
                        iterator.remove();
                    }
                    prevItem = currItem;
                }
            }
            case "country": {
                Iterator<Country> iterator = countries.iterator();
                prevItem = iterator.next().name;
                while (iterator.hasNext()) {
                    currItem = iterator.next().name;
                    if (prevItem.equals(currItem)) {
                        iterator.remove();
                    }
                    prevItem = currItem;
                }
            }
            case "subregion": {
                Iterator<Country> iterator = countries.iterator();
                prevItem = iterator.next().subregion;
                while (iterator.hasNext()) {
                    currItem = iterator.next().subregion;
                    if (prevItem.equals(currItem)) {
                        iterator.remove();
                    }
                    prevItem = currItem;
                }
            }


        }
    }



    void sortCountry(List<Country> countries, String criteria) {

        switch (criteria) {
            case "region": {
                Collections.sort(countries, new Comparator<Country>() {
                    public int compare(Country o1, Country o2) {
                        return o1.region.toString().compareTo(o2.region.toString());
                    }
                });
                break;
            }
            case "subregion": {
                Collections.sort(countries, new Comparator<Country>() {
                    public int compare(Country o1, Country o2) {
                        return o1.subregion.toString().compareTo(o2.subregion.toString());
                    }
                });
                break;
            }
            case "country": {
                Collections.sort(countries, new Comparator<Country>() {
                    public int compare(Country o1, Country o2) {
                        return o1.name.toString().compareTo(o2.name.toString());
                    }
                });
            }
        }



    }
}
