package com.example.ishtefan.geografy;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by ishtefan on 20.10.2016.
 */

public class Retrofit {

    private static final String ENDPOINT = "https://restcountries.eu/rest";
    private static ApiReference apiReference;

    static {
        initialize();
    }

    static void initialize() {


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiReference = restAdapter.create(ApiReference.class);

    }

    interface ApiReference {
        @GET("/v1/region/{region}")
        void getRegions(@Path("region") String region, Callback<List<Country>> callback);

        @GET("/v1/all")
        void getAllCountry(Callback<List<Country>> callback);

        @GET("/v1/subregion/{subregion}")
        void getSubregionsCountry(@Path("subregion") String subregion, Callback<List<Country>> callback);
    }

    public static void getRegions(String region, Callback<List<Country>> callback) {
        apiReference.getRegions(region, callback);
    }

    public static void getAllCountry(Callback<List<Country>> callback) {
        apiReference.getAllCountry(callback);
    }

    public static void getSubregionsCountry(String subregion, Callback<List<Country>> callback) {
        apiReference.getSubregionsCountry(subregion, callback);
    }
}
