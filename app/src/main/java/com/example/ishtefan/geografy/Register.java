package com.example.ishtefan.geografy;

/*
1. ТЗ. “Самоучитель географии”
        Первый спринт.
        Создать приложение Самоучитель географии. Приложение проверяет знание географии.
        На экране появляется вопрос (например Please, enter the capital of Ukraine), поле для ввода
        текста и кнопка проверки результата. Для базы знаний использовать https://restcountries.eu/

        Приложение должно иметь следующие экраны:
        Экран регистрации - экран, на котором требуется ввести данные пользователя
        (имя, пароль, страну обитания). Имя не должно быть пустым, пароль не должен быть простым,
        а страна должна быть валидной (одна из списка выше). После успешной регистрации - попадаем на
        главный экран.

        Экран логина - всегда появляется, когда пользователь залогинен. Поля для ввода логина и
        пароля и кнопка для авторизации. Если все данные совпали - попадаем на главный экран.
        Если нет - выводим сообщение об ошибке.

        Главный экран - сам опросник. Приложение случайным образом выбирает страну из subregion'а,
        в которое входит страна пользователя и спрашивает у пользователя его столицу. Если ответ
        верный, запоминает его. После чего задает новый вопрос. Как только все вопросы отвечены,
        пользователю выдается сообщение "Your knowledge is great!". Статистика ответов обнуляется
        и пользователь может заново отвечать на вопросы.

*/


import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Register extends AppCompatActivity {

    Country country;
    List<Country> countries;

    TextView name;
    TextView myCountry;
    TextView myPassword;
    Button maNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (TextView) findViewById(R.id.s_name);
        myCountry = (TextView) findViewById(R.id.s_country);
        myPassword = (TextView) findViewById(R.id.s_password);


        Retrofit.getAllCountry(new Callback<List<Country>>() {
            @Override
            public void success(final List<Country> countries, Response response) {
                Utils countryList = new Utils();
                countryList.sortCountry(countries, "country");
//              получен список стран, отсортированных. Выводим в поле со списком,

//                Spinner spinCountry;
//                spinCountry = (Spinner) findViewById(R.id.spinner);//fetch the spinner from layout file
//                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                        android.R.layout.simple_spinner_item, getResources()
//                       .getStringArray(countries);//setting the country_array to spinner
//                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//                spinCountry.setAdapter(adapter);
////if you want to set any action you can do in this listener
//                spinCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                    @Override
//                    public void onItemSelected(AdapterView<?> arg0, View arg1,
//                                               int position, long id) {
//                    }
//
//                    @Override
//                    public void onNothingSelected(AdapterView<?> arg0) {
//                    }
//                });

                maNext = (Button) findViewById(R.id.am_next_button);
                maNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(Register.this, Questions.class);
                        intent.putExtra("myName", name.getText());
                        intent.putExtra("Countries", (Serializable) countries);
                        startActivity(intent);

                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
