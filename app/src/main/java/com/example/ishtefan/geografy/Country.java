package com.example.ishtefan.geografy;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by ishtefan on 20.10.2016.
 */
public class Country implements Serializable, Parcelable {
    String region;
    String name;
    String capital;
    String subregion;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.region);
        dest.writeString(this.name);
        dest.writeString(this.capital);
        dest.writeString(this.subregion);
    }

    public Country() {
    }

    protected Country(Parcel in) {
        this.region = in.readString();
        this.name = in.readString();
        this.capital = in.readString();
        this.subregion = in.readString();
    }

    public static final Parcelable.Creator<Country> CREATOR = new Parcelable.Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel source) {
            return new Country(source);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }
}
