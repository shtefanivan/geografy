package com.example.ishtefan.geografy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

/**
 * Created by ishtefan on 01.11.2016.
 */

public class Questions extends AppCompatActivity {

    int item;
    String countryItem;

    TextView countryQuestion;
    TextView countryAnswer;
    Button aqAnswerButton;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questions);

        countryAnswer = (TextView) findViewById(R.id.capital_answer);
        countryQuestion = (TextView) findViewById(R.id.country_questions);

        final List<Country> countries = (List<Country>) getIntent().getSerializableExtra("Countries");
        final Random random = new Random();

//Столица не должна быть пустой
        do {
            item = random.nextInt(countries.size());
            countryItem = countries.get(item).getName();
        } while (countries.get(item).getCapital().equals(""));

        countryQuestion.setText(String.valueOf(countryItem));

//        Log.d("!!!!!!!!!!!!", String.valueOf(countries.toString()));
        aqAnswerButton = (Button) findViewById(R.id.aq_answer_button);

        aqAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Capital Question", countries.get(item).getCapital());
                Log.d("Capital Answer", String.valueOf(countryAnswer.getText()));
                if (countries.get(item).getCapital().equals(String.valueOf(countryAnswer.getText()))) {
                    Toast.makeText(Questions.this, "Отлично!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(Questions.this, "Поробуйте еще!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
